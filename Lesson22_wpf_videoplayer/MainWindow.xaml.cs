﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace WpfVideoPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool isHidden = false;
        private bool isStoped = false;
        private List<string> playlist = new List<string>();
        private GridLength tempWidth;

        public MainWindow()
        {
            InitializeComponent();
            var gridView = new GridView();
            this.listViewPlayList.View = gridView;
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Playlist",
                DisplayMemberBinding = new Binding("Name"),
                Width = listViewPlayList.Width
            });
        }

        private void Window_Loaded(object sender, RoutedEventArgs ea)
        {
            _media.ScrubbingEnabled = true;
            _media.Volume = slVolume.Value;
            //listViewPlayList.SelectionMode = SelectionMode.Single;
        }

        private void _menuOpen(object sender, RoutedEventArgs ea)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All Files (*.*)|*.*|Video Files (*.avi)|*.avi|Video Files (*.mp4)|*.mp4|Video Files (*.flv)|*.flv";
            dlg.FilterIndex = 2;

            bool? res = dlg.ShowDialog();
            if (res.HasValue && res.Value)
            {
                playlist.Add(dlg.FileName);
                FileInfo fi = new FileInfo(playlist.Last());
                var item = new { Name = fi.Name };
                listViewPlayList.SelectedItems.Clear();
                listViewPlayList.Items.Add(item);
                listViewPlayList.SelectedItems.Add(item);

                _media.MediaOpened += _media_MediaOpened;
                //_media.Source = new Uri(dlg.FileName, UriKind.Absolute);
                _media.Clock = new MediaTimeline(new Uri(dlg.FileName, UriKind.Absolute)).CreateClock();
                _media.Clock.CurrentTimeInvalidated += Clock_CurrentTimeInvalidated;
                //_media.Play();
            }
        }
        private void _menuExit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Clock_CurrentTimeInvalidated(object sender, EventArgs e)
        {
            _mediaProgress.Value = _media.Position.TotalSeconds;
            curTime.Content =TimeSpan.FromSeconds(_media.Position.Seconds);
        }

        private void _media_MediaOpened(object sender, RoutedEventArgs e)
        {
            _mediaProgress.Maximum = _media.NaturalDuration.TimeSpan.TotalSeconds;
            durTime.Content = _media.NaturalDuration.TimeSpan;
        }

        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            if(isStoped == true)
            {
                isStoped = false;
                btnPause.Content = "Pause";
                _media.Clock.Controller.Begin();
            }
            else if (_media.Clock.IsPaused)
            {
                _media.Clock.Controller.Resume();
                btnPause.Content = "Pause";
            }
            else
            {
                _media.Clock.Controller.Pause();
                btnPause.Content = "Play";
            }

        }
        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            isStoped = true;
            _media.Clock.Controller.Stop();
            btnPause.Content = "Play";

        }
        private void BtnBackwPlay_Click(object sender, RoutedEventArgs e)
        {
            _media.Clock.Controller.Seek(_media.Position - TimeSpan.FromSeconds(30), TimeSeekOrigin.BeginTime);
        }
        private void BtnForwPlay_Click(object sender, RoutedEventArgs e)
        {
            _media.Clock.Controller.Seek(_media.Position + TimeSpan.FromSeconds(30), TimeSeekOrigin.BeginTime);
        }
        private void BtnPrev_Click(object sender, RoutedEventArgs e)
        {
            int pos = listViewPlayList.SelectedIndex;
            int count = listViewPlayList.Items.Count;
            if (listViewPlayList.SelectedIndex - 1 >= 0)
            {
                listViewPlayList.SelectedItems.Clear();
                listViewPlayList.SelectedItems.Add(listViewPlayList.Items[pos - 1]);
            }
            else
            {
                listViewPlayList.SelectedItems.Clear();
                listViewPlayList.SelectedItems.Add(listViewPlayList.Items[count - 1]);
            }
            playSelected();
        }
        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            int pos = listViewPlayList.SelectedIndex;
            int count = listViewPlayList.Items.Count;
            if (listViewPlayList.SelectedIndex + 1 < count)
            {
                listViewPlayList.SelectedItems.Clear();
                listViewPlayList.SelectedItems.Add(listViewPlayList.Items[pos + 1]);
            }
            else
            {
                listViewPlayList.SelectedItems.Clear();
                listViewPlayList.SelectedItems.Add(listViewPlayList.Items[0]);
            }
            playSelected();
        }



        private void BtnHide_Click(object sender, RoutedEventArgs e)
        {
            if (isHidden == false)
            {
                btnHide.Content = "<Show";
                tempWidth = mediaColumn.Width;
                mediaColumn.Width = new GridLength(MWindow.Width);
                isHidden = true;
            }
            else if (isHidden == true)
            {
                btnHide.Content = "Hide>";
                //mediaColumn.Width = tempWidth;
                mediaColumn.Width = new GridLength(590);
                isHidden = false;
            }
            _media.Width = mediaColumn.Width.Value;

        }

        private void _mediaProgress_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                double x = e.GetPosition(_mediaProgress).X;
                double pos = x * 100 / _mediaProgress.ActualWidth;
                TimeSpan ts = TimeSpan.FromSeconds(_media.Clock.NaturalDuration.TimeSpan.TotalSeconds / 100.0 * pos);
                _media.Clock.Controller.Seek(ts, TimeSeekOrigin.BeginTime);
            }
            catch(Exception ex) { }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _media.Volume = (double)slVolume.Value;
        }

        private void ListView_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            List<string> fileList = new List<string>(files);

            foreach(var file in fileList)
            {
                FileInfo fi = new FileInfo(file);
                if(fi.Name.Contains(".avi") | fi.Name.Contains(".mp4")) // потом добавлю ещё
                {
                    var item = new { Name = fi.Name };
                    this.listViewPlayList.Items.Add(item);
                    playlist.Add(file);
                }
            }
        }

        private void playSelected(object sender, MouseButtonEventArgs e)
        {
            playSelected();
        }

        private void playSelected()
        {
            try
            {
                string selectedItem = listViewPlayList.SelectedItems[0].ToString().Split(new string[] { "{", "=", "}" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim();
                string fullName = string.Empty;
                foreach (var file in playlist)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.Name.Equals(selectedItem))
                    {
                        fullName = fi.FullName;
                    }
                }
                _media.MediaOpened += _media_MediaOpened;
                _media.Clock = new MediaTimeline(new Uri(fullName, UriKind.Absolute)).CreateClock();
                _media.Clock.CurrentTimeInvalidated += Clock_CurrentTimeInvalidated;
                btnPause.Content = "Pause";
            }
            catch (Exception ex) { }
            
        }
    }
}
